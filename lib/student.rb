class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_accessor :courses
  attr_reader :first_name, :last_name
  
  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    courses.each do |course|
      raise "Conflicts with schedule" if course.conflicts_with?(new_course)
    end

    courses << new_course unless courses.include?(new_course)
    new_course.students << self
  end

  def course_load
    department_and_credits = Hash.new(0)
    courses.each do |course|
      department_and_credits[course.department] += course.credits
    end

    department_and_credits
  end
end
